package edu.towson.cosc431.labsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.graphics.Bitmap
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.app.Activity
import android.content.Intent
import android.util.Log
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
class MainActivity : AppCompatActivity(), IController {
    override fun getIcon(iconUrl: String, callback: (Bitmap) -> Unit) {
        NetworkHelper()
            .fetchIcon(iconUrl, callback)
    }

    companion object {
        val TAG = MainActivity::class.java.simpleName
    }

    val mutablePeople: MutableList<Person> = mutableListOf()
    override val people: List<Person> = mutablePeople
    //broadcast receiver
    val receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(ctx: Context?, intent: Intent?) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // TODO - 7. Make a network call (GET) to fetch the list of people from https://my-json-server.typicode.com/rvalis-towson/lab_api/people
        download()
    }

    //todo Register
    override fun onResume() {
        super.onResume()
        registerReceiver(receiver, IntentFilter(MyIntentService.BROADCAST_ACTION))

    }

    //todo unregister
    override fun onPause() {
        super.onPause()
        unregisterReceiver(receiver)
    }

    fun download() {
        val thread = Thread(Runnable {
            // perform some "work"
            Thread.sleep(10000)

            this@MainActivity.runOnUiThread {
                // this will now work since we queue this Runnable on the UI queue
                NetworkHelper()
                    .fetchPeople { people ->
                        mutablePeople.clear()
                        mutablePeople.addAll(people)
                        recyclerView.adapter?.notifyDataSetChanged()
                    }
                recyclerView.adapter = PersonAdapter(this)
                recyclerView.layoutManager = LinearLayoutManager(this)

            }

        })
        thread.start()
    }

}

interface IController {
    fun getIcon(iconUrl: String, callback: (Bitmap) -> Unit)
    val people: List<Person>
}