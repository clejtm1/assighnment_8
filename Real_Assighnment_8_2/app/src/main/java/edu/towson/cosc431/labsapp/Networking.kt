package edu.towson.cosc431.labsapp

import javax.security.auth.callback.Callback
import android.graphics.Bitmap
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.BitmapRequestListener
import com.androidnetworking.interfaces.ParsedRequestListener
import java.lang.Exception
import android.accessibilityservice.GestureDescription
import android.app.IntentService
import android.app.PendingIntent
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
class NetworkHelper {
    // TODO - 5. Implement a method for fetching the person list

companion object{ val API_URL="https://my-json-server.typicode.com/rvalis-towson/todos_api/todos"}

        fun fetchPeople(callback: (List<Person>)->Unit) {
            AndroidNetworking.get(API_URL)
                .setTag(this)
                .setPriority(Priority.LOW)
                .build()
                .getAsObjectList(Person::class.java, object : ParsedRequestListener<List<Person>> {
                    override fun onResponse(response: List<Person>?) {
                        if (response != null) {
                            callback(response)
                        } else {
                            throw Exception("Error Fetching People")
                        }
                    }

                    override fun onError(anError: ANError?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }
                })
        }
    // TODO - 6. Implement a method for fetching the person icon
    fun fetchIcon(iconUrl:String,callback: (Bitmap)->Unit)
    {
        AndroidNetworking.get(API_URL)
            .setTag("imageRequestTag")
            .setPriority(Priority.LOW)
            .setBitmapMaxHeight(400)
            .setBitmapMaxWidth(400)
            .setBitmapConfig(Bitmap.Config.ARGB_8888)
            .build()
            .getAsBitmap(object: BitmapRequestListener{
                override fun onResponse(response: Bitmap?) {
                    if (response != null) {
                        callback(response)
                    } else {
                        throw Exception("Error Fetching Icons")
                    }
                }

                override fun onError(anError: ANError?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            })
    }
}