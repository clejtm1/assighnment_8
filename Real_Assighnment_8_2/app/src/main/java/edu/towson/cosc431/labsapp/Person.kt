package edu.towson.cosc431.labsapp
import com.google.gson.annotations.SerializedName
// TODO - 4. Use Gson's SerializedName to map the json property name to your Person property name
data class Person(
    @SerializedName("id")
    val Id: Int,
    @SerializedName("title")
    val Title: String,
    @SerializedName("contents")
    val Contents: String,
    @SerializedName("completed")
    val Completed: Boolean,
    @SerializedName("image_url")
    val iconUrl: String)
